drop table if exists evaluation_calculate_log;
create table evaluation_calculate_log
(
id            varchar(32) not null,

report_id varchar(50)  comment '报告id',
drug_id varchar(50)  comment '产品Id',
generic_name text  comment '通用名称',
manufacture text  comment '生产厂家',
adverse_event_id varchar(50)  comment '不良事件ID',
event_name text  comment '不良事件名称',
evaluation_id varchar(50)  comment '分类key',
need_calculate tinyint  default '0'  comment '是否需要重新计算（1:需要重新计算，0:不需要重新计算）',

tenant_id     varchar(50) comment '租户Id',
version       bigint      not null,
create_time   datetime(6),
create_by     varchar(32),
update_time   datetime(6),
update_by     varchar(32),
is_deleted    tinyint,
primary key (id)
)  COMMENT = '报告评价计算标志';

create   index idx_tenantid on evaluation_calculate_log (tenant_id ASC);